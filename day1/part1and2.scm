#|
So what's the deal here?

Looks like I am given a nice story to get some motivation. But TBH, I
cannot relate to Christmas vacations. Never had one. Anyway, looks
like I have skipped 5 vacations - what am I a nerd?

There's MIT/Gnu Scheme Reference Manual in `~/refs` just in case.

A cash-only society on a tropical island. Alright, good planning
alternate version of me! Cash is gold coins called `stars`. I need to
find 50 of these "stars" by the time I reach the island to pay as
_deposit_ of the room. That is really weird, I gotta pay deposits in
an economy that runs cash-only, and exchanges have not heard of them?
Looks like economics is an emergent phenomenon. Anyway, 50 stars by
25th of December, 2 each day for 25 days. Cool. I will try to stick to
this routine though it may get out of hand. Presumably this is
designed for people getting 25 days of vacations which would allow
them to spend quite some time figuring things out. I am already late
and have a product to make - cut me some slack?

The stars are collected by solving puzzels. Two new puzzles everyday;
second puzzle is unlocked after solving the first.

----

Puzzle: Fix your expense report.

1. Find _the_ two entries that sum to 2020.
2. Multiply the two numbers

|#


#|
---------------------------------------------
First, read the input file
---------------------------------------------
I did not know how to read from files.

Reading from the reference and some stackoverflow stuff, turns out it
is awesome.

Open a port, and (read-line) just like you would read a line from
anywherer else. Since I wanted all the entries, I am reading them in a
loop. I did not know (let loop) form either.

|#
(define input-file "input.txt")

(define example-input
  (with-input-from-file input-file
    (lambda ()
      (let loop ((lines '())
                 (next-line (read-line)))
        (if (eof-object? next-line)
            (reverse lines)
            (loop (cons (string->number (string-trim next-line))
                        lines)
                  (read-line)))))))

#| 
Sum the list entries, if 200, multiply 

I went through way too many iterations on this one.

I like said before, I did not know looping constructs of Scheme. First
I tried with `do` and it got so weird after some time that I was
confused what I was doing. Asking #scheme (freenode) about this,
people advised me not to use `do` form, and instead go with something
like `for-each` or `named let`.

My first working solution was two nested `for-each`. It seemed to be
working and I went to sleep!

Next morning I declared my victory in #scheme and #emacs. #emacs
pointed out at least 1 bug and 1 improvement in the code. Bug was that
I was summing the element with itself too - this is not to be
done. Improvement was to not loop through the entire list in the inner
loop. Another improvement I wanted to make was to break the loop when
the answer was found.

Given these, I set out to make `call-with-current-continuation`
work. When I grasped how the approach is supposed to behave, it was
neat and simple. You can see the whole form is under
`call/cc` (`call/cc` is abbreviation of
`call-with-current-continuation`).

To solve the bug and improve iterations, I have decided to convert
inner loop to a named let expression. This lets me pass `cdr` of the
list progressively. This is still not optimal. The inner loop starts
with (n - 1) length list every time. This can be shortened further.

|#
(call/cc
 (lambda (exit)
   (for-each (lambda (entry-one)
               (let loop ((entries (cdr example-input)))
                 (cond ((null? entries)
                        'nil)
                       ((eq? (+ entry-one (car entries)) 2020)
                        (exit (* entry-one (car entries))))
                       (else
                        (loop (cdr entries))))))
             example-input)
   #t))

#|
Alright! Here it is, the improved outer loop.

With for-each, I think it could not be done - at least simply. So I
converted it to named let. Now the inner loop operates over
the "remaining" elements. Here is what I mean:

Say the input is `(1 2 3 4)`.
Outer loop starts with `1`.
Inner loop starts with `(2 3 4)`.

Outer loop starts with `2`.
Inner loop starts with `(3 4)`.

This is better because in the first iteration, we already summed 1 +
2. There is no point in summing 2 + 1 when the outer loop goes for 2.

|#

(call/cc
 (lambda (exit)
   (let outer-loop ((outer-entries example-input)
                    (outer-entry (car example-input)))
     (cond ((null? outer-entries)
            'nil)
           (else
            (let inner-loop ((inner-entries (cdr outer-entries)))
              (cond ((null? inner-entries)
                     'nil)
                    ((eq? (+ outer-entry (car inner-entries)) 2020)
                     (exit (* outer-entry (car inner-entries))))
                    (else
                     (inner-loop (cdr inner-entries)))))
            (outer-loop (cdr outer-entries) (car outer-entries)))))
   #t))


#| Now for the second part
----
Puzzle: Fix your expense report.
1. Find _the_ THREE entries that sum to 2020.
2. Multiply the three numbers

Literally took me 10min of renaming and adding the yet another inner
loop.

|#

(call/cc
 (lambda (exit)
   (let loop-one ((entries-one example-input)
                  (entry-one (car example-input)))
     (cond ((null? entries-one)
            'nil)
           (else
            (let loop-two ((entries-two (cdr entries-one))
                           (entry-two (car entries-one)))
              (cond ((null? entries-two)
                     'nil)
                    (else
                     (let loop-three ((entries-three (cdr entries-two))
                                      (entry-three (car entries-two)))
                       (cond ((null? entries-three)
                              'nil)
                             ((eq? (+ entry-one entry-two entry-three) 2020)
                              (exit (* entry-one entry-two entry-three)))
                             (else
                              (loop-three (cdr entries-three) (car entries-three)))))
                     (loop-two (cdr entries-two) (car entries-two)))))
            (loop-one (cdr entries-one) (car entries-one)))))
   #t))

#|

Now we are here, trying to get going on our toboggan. It is madness, I
still say, but anyway.

Listen to this one:
"Due to the local geology, trees in this area only grow on exact
integer coordinates in a grid."

*I* think some humans are farming here! No way arboreal genetics and
stuff has caused this.

--------

Puzzle:

Given a grid of repeating pattern (trees and land map), find out how
many trees the toboggan will encounter while crossing the entire grid
from the starting position of 1,1.

--------

I could think of two approaches:

1. Modulo arithmetic
2. Circular lists

I really like circular lists, especially because they're nice side
effect of the language's design. Hence, that's what I will use.

Bird's Eye view of the logic:
- Start at position (1,1) -> (row, col)
- Calculate next step by adding (3, 1)
- If the position is a tree, increment counter
- Keep at it till the end of the grid

Neat? I think so! Simple as well.

#scheme suggested `(circular-list)` takes objects. It's definition
says `(circular-list . objects)` which means any number of
objects. This is excellent because I can `apply` circular-list to
`string->list` and get a nice circular row of trees/land grid.

|#

(define input-file "input.txt")

(define example-input
  ;; the structure is going to be:
  ;; (#0=(#\. #\. #\# #\# #\. #\. #\. #\. #\. #\. #\. . #0#)
  ;;  #1=(... . #1#)
  ;;  ...)
  ;; i.e., a list of circular lists of the form
  ;;       ((row-1) (row-2) (row-3) ...)
  (with-input-from-file input-file
    (lambda ()
      (let loop ((lines '())
                 (next-line (read-line)))
        (if (eof-object? next-line)
            (reverse lines)
            ;; Generate the circular list right here, because I don't
            ;; yet have a generic (read-lines) procedure or anything
            ;; like that defined.
            (loop (cons (apply circular-list (string->list (string-trim next-line)))
                        lines)
                  (read-line)))))))

(define (cross-the-area forest)
  ;; Some variables to keep track of things
  (define current-position (cons 0 0))
  (define steps (list
                 (cons 1 1)
                 (cons 3 1)
                 (cons 5 1)
                 (cons 7 1)
                 (cons 1 2)))
  (define (take-next-step pos step)
    (cons (+ (car step) (car pos))
          (+ (cdr step) (cdr pos))))
  (define (is-tree? pos)
    (char=? #\#
            (list-ref
             (list-ref example-input (cdr pos))
             (car pos))))
  (apply *
         (fold-left
          (lambda (trees step)
            ;; Reset current position for each step
            (let ((current-position (cons 0 0)))
              (append
               trees
               (list
                (fold-left
                 (lambda (trees-encountered row)
                   (set! current-position (take-next-step current-position step))
                   (cond ((and (< (cdr current-position)
                                  (length example-input))
                               (is-tree? current-position))
                          (+ 1 trees-encountered))
                         (else
                          trees-encountered)))
                 0
                 example-input)))))
          '()
          steps)))

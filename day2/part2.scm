#|

People make mistakes. So did the shopkeeper, evidently. The policy I
was given was Somebody Else's Problem Field. Oh well.

So there's a new policy that requires minimal change to our earlier
approach from part 1.

Instead of checking `at-least?` and `at-most?`, we now have
`appears-once?`. Neat yeah?

First, I tried the regex approach. But I did not like how it was
becoming a mix of regex searches and string manipulations. Here is
what I mean:

(apply substring "1-3 a: abcde"
       (try-match '(seq (start-string)
                        (+ (char-in numeric))
                        "-"
                        (+ (char-in numeric)))
                  "1-3 a: abcde"))

The code above gives us "1-3" which is still a damn string that needs
manipulation.

Suppose we trivially modify that code into giving us just "1" which
can be (string->number). Then what? How do we arrive at number 3
without doing string manipulations on the results of (try-match)? I
couldn't think of any elegant way.

So here we are, using the same old approach because I like it.

|#


(define input-file "input.txt")

(define example-input
  (with-input-from-file input-file
    (lambda ()
      (let loop ((lines '())
                 (next-line (read-line)))
        (if (eof-object? next-line)
            (reverse lines)
            (loop (cons (string-trim next-line)
                        lines)
                  (read-line)))))))

(define (transform-string-input-to-datastruct input)
  (define (get-policy-positions input)
    (cons (string->number (car ((string-splitter 'delimiter #\-)
                                (car
                                 ((string-splitter 'delimiter #\space)
                                  (car ((string-splitter 'delimiter #\:) input)))))))
          (string->number (cadr ((string-splitter 'delimiter #\-)
                                 (car
                                  ((string-splitter 'delimiter #\space)
                                   (car ((string-splitter 'delimiter #\:) input)))))))))
  (define (get-policy-char input)
    (cadr
     ((string-splitter 'delimiter #\space)
      (car ((string-splitter 'delimiter #\:) input)))))
  (define (get-policy-password input)
    (string-trim (cadr
                  ((string-splitter 'delimiter #\:) input))))
  (list (get-policy-positions input)
        (get-policy-char input)
        (get-policy-password input)))

(define (validate-passwords)
  (let ((valid-passwords-count 0))
    (define (get-first-pos data)
      (- (caar data) 1))
    (define (get-second-pos data)
      (- (cdar data) 1))
    (define (get-validation-word data)
      (cadr data))
    (define (get-password data)
      (caddr data))
    (define (appears-once? pos-one pos-two word password)
      (not (equal? (string=? word (char->name (string-ref password pos-one)))
                   (string=? word (char->name (string-ref password pos-two))))))
    (define (is-valid? data)
      (appears-once? (get-first-pos data)
                     (get-second-pos data)
                     (get-validation-word data)
                     (get-password data)))
    (fold-left (lambda (sum line)
                 (let ((data (transform-string-input-to-datastruct line)))
                   (if (is-valid? data)
                       (+ 1 sum)
                       sum)))
               0
               example-input)))

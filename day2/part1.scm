#|
Here we go, second day!

Those Elves in accounting are now happy, they gave me 2 stars.

It is clear from the second day, that the alternate reality me is
*very* nosy and crazy. Who asks if one can have a look into a problem
where people cannot login?

Anyway, I would have asked them to do two things:
1. Turn it off and on again
2. Get their DB admin to change every single password to a default,
   and mandate a password reset policy.

How do they have access to plaintext password anyway? Some world
people would fight all day long in!

---

Puzzle:
1. Find out corrupt passwords given the policies
2. How many passwords are corrupt?

Instead of ranting or making snarky remarks about corrupt databases,
let's focus on the task at hand shall we?

There are 2 potential approaches I can think of:

1. Generate a regex out of the policy and run it on the password
2. Do `car`, `cdr` and string matching magic

|#

(define input-file "input.txt")

(define example-input
  (with-input-from-file input-file
    (lambda ()
      (let loop ((lines '())
                 (next-line (read-line)))
        (if (eof-object? next-line)
            (reverse lines)
            (loop (cons (string-trim next-line)
                        lines)
                  (read-line)))))))

;; TODO: learn `define-syntax` for all the string-splitter things
(define (transform-string-input-to-datastruct input)
  (define (get-policy-min-max input)
    (cons (string->number (car ((string-splitter 'delimiter #\-)
                                (car
                                 ((string-splitter 'delimiter #\space)
                                  (car ((string-splitter 'delimiter #\:) input)))))))
          (string->number (cadr ((string-splitter 'delimiter #\-)
                                 (car
                                  ((string-splitter 'delimiter #\space)
                                   (car ((string-splitter 'delimiter #\:) input)))))))))
  (define (get-policy-char input)
    (cadr
     ((string-splitter 'delimiter #\space)
      (car ((string-splitter 'delimiter #\:) input)))))
  (define (get-policy-password input)
    (string-trim (cadr
                  ((string-splitter 'delimiter #\:) input))))
  (list (get-policy-min-max input)
        (get-policy-char input)
        (get-policy-password input)))

(define (validate-passwords)
  (let ((valid-passwords-count 0))
    (define (get-lower-bound data)
      (caar data))
    (define (get-upper-bound data)
      (cdar data))
    (define (get-validation-word data)
      (cadr data))
    (define (get-password data)
      (caddr data))
    (define (atleast? count char str)
      (>= (length (string-search-all char str))
          count))
    (define (atmost? count char str)
      (<= (length (string-search-all char str))
          count))
    (define (is-valid? data)
      (and (atleast? (get-lower-bound data)
                     (get-validation-word data)
                     (get-password data))
           (atmost? (get-upper-bound data)
                    (get-validation-word data)
                    (get-password data))))
    (fold-left (lambda (sum line)
                 (let ((data (transform-string-input-to-datastruct line)))
                   (if (is-valid? data)
                       (+ 1 sum)
                       sum)))
               0
               example-input)))
